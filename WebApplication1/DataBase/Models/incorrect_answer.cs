﻿using System;
using System.Collections.Generic;

namespace WebApplication1.DataBase.Models
{
    public partial class incorrect_answer
    {
        public int incorrect_answers_id { get; set; }
        public string wrong_choice { get; set; } = null!;
        public int quiz_id { get; set; }

        public virtual quiz quiz { get; set; } = null!;
    }
}
