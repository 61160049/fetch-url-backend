﻿using System;
using System.Collections.Generic;

namespace WebApplication1.DataBase.Models
{
    public partial class quiz
    {
        public quiz()
        {
            incorrect_answers = new HashSet<incorrect_answer>();
        }

        public int quiz_id { get; set; }
        public string category { get; set; } = null!;
        public string type { get; set; } = null!;
        public string difficulty { get; set; } = null!;
        public string question { get; set; } = null!;
        public string correct_answer { get; set; } = null!;

        public virtual ICollection<incorrect_answer> incorrect_answers { get; set; }
    }
}
