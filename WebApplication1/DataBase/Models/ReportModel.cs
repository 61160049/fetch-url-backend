﻿namespace WebApplication1.DataBase.Models
{
    public class ReportList
    {
        public List<ReportModel> reportList { get; set; }
    }

    public class ReportModel
    {
        public int report_id { get; set; }
        public string category { get; set; }
        public int total_quiz { get; set; }
        public int correct_count { get; set; }
        public int incorrect_count { get; set; }
    }
}
