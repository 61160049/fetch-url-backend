﻿using System;
using System.Collections.Generic;

namespace WebApplication1.DataBase.Models
{
    public partial class report
    {
        public int report_id { get; set; }
        public string category { get; set; } = null!;
        public int? total_quiz { get; set; }
        public int? correct_count { get; set; }
        public int? incorrect_count { get; set; }
    }
}
