﻿using System.ComponentModel.DataAnnotations;

namespace WebApplication1.DataBase.Models
{
    public class APIResult
    {
        public APIResult()
        {
            results = new List<ResultModel>();
        }
        public List<ResultModel> results { get; set; } 
        
    }
    public class ResultModel
    {
        [Required]
        public int quiz_id { get; set; }
        [Required]
        public string category { get; set; }
        [Required]
        public string type { get; set; }
        [Required]
        public string difficulty { get; set; }
        [Required]
        public string question { get; set; }
        [Required]
        public string correct_answer { get; set; }
        public List<IncorrectModel> incorrect_answers { get; set; }
    }
    public class IncorrectModel
    {
        public int incorrect_answers_id { get; set; }
        public string wrong_choice { get; set; } = null!;
        
    }

    public class ResultStatus
    {
        public int id { get; set; }
        public string status { get; set; }
    }
}
