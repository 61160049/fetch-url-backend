﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using WebApplication1.DataBase.Models;

namespace WebApplication1.DataBase.Context
{
    public partial class ExampleDbContext : DbContext
    {
        public ExampleDbContext()
        {
        }

        public ExampleDbContext(DbContextOptions<ExampleDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<incorrect_answer> incorrect_answers { get; set; } = null!;
        public virtual DbSet<quiz> quizzes { get; set; } = null!;
        public virtual DbSet<report> reports { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseMySql("server=127.0.0.1;port=3306;uid=root;password=Sirmetaknight147!;database=sample", Microsoft.EntityFrameworkCore.ServerVersion.Parse("8.0.27-mysql"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.UseCollation("utf8mb4_0900_ai_ci")
                .HasCharSet("utf8mb4");

            modelBuilder.Entity<incorrect_answer>(entity =>
            {
                entity.HasKey(e => e.incorrect_answers_id)
                    .HasName("PRIMARY");

                entity.HasIndex(e => e.quiz_id, "quiz_id_idx");

                entity.Property(e => e.wrong_choice).HasMaxLength(100);

                entity.HasOne(d => d.quiz)
                    .WithMany(p => p.incorrect_answers)
                    .HasForeignKey(d => d.quiz_id)
                    .HasConstraintName("quiz_id");
            });

            modelBuilder.Entity<quiz>(entity =>
            {
                entity.HasKey(e => e.quiz_id)
                    .HasName("PRIMARY");

                entity.ToTable("quiz");

                entity.HasIndex(e => e.question, "question_UNIQUE")
                    .IsUnique();

                entity.Property(e => e.category).HasMaxLength(45);

                entity.Property(e => e.correct_answer).HasMaxLength(100);

                entity.Property(e => e.difficulty).HasMaxLength(45);

                entity.Property(e => e.type).HasMaxLength(45);
            });

            modelBuilder.Entity<report>(entity =>
            {
                entity.HasKey(e => e.report_id)
                    .HasName("PRIMARY");

                entity.ToTable("report");

                entity.Property(e => e.category).HasMaxLength(100);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
