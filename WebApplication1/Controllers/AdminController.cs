﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Web;
using WebApplication1.DataBase.Context;
using WebApplication1.DataBase.Models;

namespace WebApplication1.Controllers
{
    public class AdminController : ControllerBase
    {
        readonly ExampleDbContext _context;
        public AdminController(IHttpClientFactory httpClientFactory, ExampleDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var questionList = (from q in _context.quizzes
                                orderby q.quiz_id
                                select new { q.quiz_id, q.question }).ToList();
            return Ok(questionList);
        }

        [HttpGet]
        public IActionResult GetQuestionList([FromQuery] ResultModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var questionList = (from q in _context.quizzes
                                where (q.category == model.category || String.IsNullOrEmpty(model.category))
                                && (q.difficulty == model.difficulty || String.IsNullOrEmpty(model.difficulty))
                                && (q.type == model.type || String.IsNullOrEmpty(model.type))
                                && (q.question.Contains(model.question) || String.IsNullOrEmpty(model.question))
                                orderby q.quiz_id
                                select new { q.quiz_id, q.category, q.difficulty, q.type, q.question }).AsNoTracking().ToList(); //can make a new model to replace select new
            return new JsonResult(questionList);
        }

        [HttpGet]
        public IActionResult GetQuestionInfo(int id)
        {
            var question = _context.quizzes.Single(q => q.quiz_id == id);
            ResultModel model = new ResultModel();
            model.quiz_id = question.quiz_id;
            model.category = question.category;
            model.type = question.type;
            model.difficulty = question.difficulty;
            model.question = question.question;
            model.correct_answer = question.correct_answer;
            model.incorrect_answers = (from i in _context.incorrect_answers
                                       where i.quiz_id == id
                                       select new IncorrectModel {
                                           incorrect_answers_id = i.incorrect_answers_id,
                                           wrong_choice = i.wrong_choice
                                       }).ToList();
            return new JsonResult(model);
        }

        [HttpPost]
        public IActionResult CreateQuestion([FromBody] ResultModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (var dbTrans = _context.Database.BeginTransaction())
            {
                try
                {
                    var checkDup = from d in _context.quizzes where d.question == model.question select d;
                    if (checkDup.Any())
                    {
                        return Ok("There's a duplicate value in list.");
                    }
                    else
                    {
                        quiz quiz = new quiz
                        {
                            category = model.category,
                            type = model.type,
                            difficulty = model.difficulty,
                            question = model.question,
                            correct_answer = model.correct_answer
                        };
                        _context.quizzes.Add(quiz);
                        _context.SaveChanges();
                        int id = quiz.quiz_id;
                        //for (var i = 0; i < model.incorrect_answers.Count; i++)
                        //{
                        //    incorrect_answers wrong = new incorrect_answers()
                        //    {
                        //        //wrong_choice = model.incorrect_answers[i],
                        //        quiz_id = id
                        //    };
                        //    _context.Add(wrong);                           
                        //}
                        foreach (var item in model.incorrect_answers)
                        {
                            _context.incorrect_answers.Add(new incorrect_answer
                            {
                                //incorrect_answers_id = item.incorrect_answers_id,
                                wrong_choice = item.wrong_choice,
                                quiz_id = id
                            });
                        }
                        ResultStatus status = new ResultStatus
                        {
                            id = quiz.quiz_id,
                            status = "Create Success!"
                        };
                        _context.SaveChanges();
                        dbTrans.Commit();
                        return Ok(status);
                    }
                }
                catch (Exception ex)
                {
                    dbTrans.Rollback();
                    return BadRequest(ex.Message);
                }
            }
        }

        [HttpPost]
        public IActionResult ModifyQuestion([FromBody] ResultModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (var dbTrans = _context.Database.BeginTransaction())
            {
                try
                {
                    var updateQuiz = _context.quizzes.SingleOrDefault(q => q.quiz_id == model.quiz_id);
                    if (updateQuiz != null)
                    {
                        updateQuiz.category = model.category;
                        updateQuiz.type = model.type;
                        updateQuiz.difficulty = model.difficulty;
                        updateQuiz.question = model.question;
                        updateQuiz.correct_answer = model.correct_answer;
                        if (model.incorrect_answers.Any())
                        {
                            var updateIncorrectAnswers = (from i in _context.incorrect_answers
                                                          where i.quiz_id == model.quiz_id
                                                          select i).ToList();
                            List<incorrect_answer> update = new List<incorrect_answer>();
                            List<incorrect_answer> delete = new List<incorrect_answer>(updateIncorrectAnswers);
                            List<incorrect_answer> incorrect = new List<incorrect_answer>();
                            foreach (var item in model.incorrect_answers)
                            {
                                if (item.incorrect_answers_id == 0) // fetch doesn't give an id
                                {
                                    incorrect.Add(new incorrect_answer
                                    {
                                        wrong_choice = item.wrong_choice,
                                        quiz_id = model.quiz_id
                                    });
                                }
                                else
                                {
                                    var itemFromDatabaseIndex = updateIncorrectAnswers.FindIndex(x => x.incorrect_answers_id == item.incorrect_answers_id);
                                    var itemFromDatabase = updateIncorrectAnswers[itemFromDatabaseIndex];
                                    itemFromDatabase.wrong_choice = item.wrong_choice;
                                    update.Add(itemFromDatabase);
                                    delete.Remove(itemFromDatabase);
                                }
                            }
                            //foreach (var item in update)
                            //{
                            //    var itemFromDatabaseIndex = updateIncorrectAnswers.FindIndex(x => x.incorrect_answers_id == item.incorrect_answers_id);
                            //    var itemFromDatabase = updateIncorrectAnswers[itemFromDatabaseIndex];

                            //    itemFromDatabase.wrong_choice = item.wrong_choice;
                            //}
                            _context.SaveChanges();
                            _context.incorrect_answers.RemoveRange(delete);
                            _context.incorrect_answers.AddRange(incorrect);
                        }
                    }
                    _context.SaveChanges();
                    dbTrans.Commit();
                    int FindIndex(int id, List<incorrect_answer> arr)
                    {
                        for(var i = 0; i < arr.Count; i++)
                        {
                            if (arr[i].incorrect_answers_id == id)
                            {
                                return i;
                            }
                        }
                        return -1;
                    }
                    return Ok("Update Complete");
                } catch (Exception ex)
                {
                    dbTrans.Rollback();
                    return Ok("Update Failed");
                }
            }
        }

        [HttpPost]
        public IActionResult DeleteQuestion([FromBody] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (var dbTrans = _context.Database.BeginTransaction())
            {
                try
                {
                    var quizToRemove = _context.quizzes.SingleOrDefault(x => x.quiz_id == id);

                    if (quizToRemove != null)
                    {
                        var removeIncorrect = (from i in _context.incorrect_answers
                                               where i.quiz_id == quizToRemove.quiz_id
                                               select i).ToList();
                        if (removeIncorrect.Count > 0)
                            _context.incorrect_answers.RemoveRange(removeIncorrect);
                        _context.quizzes.Remove(quizToRemove);
                        _context.SaveChanges();
                        dbTrans.Commit();
                        return Ok("Delete Complete");
                    }
                    else
                    {
                        return Ok("There's no data in the first place.");
                    }
                }
                catch (Exception ex)
                {
                    dbTrans.Rollback();
                    return Ok("Delete Failed");
                }
            }
        }
        [HttpPost]
        public IActionResult UpdateReport([FromBody] ReportList list)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (var dbTrans = _context.Database.BeginTransaction())
            {
                try
                {
                    var categoryInQuiz = (from q in _context.quizzes select q.category).Distinct().ToList();
                    var categoryInReport = (from r in _context.reports select r.category).ToList();
                    List<string> check = new List<string>(categoryInQuiz.Except(categoryInReport));
                    foreach (var item in check)
                    {
                        report report = new report
                        {
                            category = item,
                            total_quiz = 0,
                            correct_count = 0,
                            incorrect_count = 0,
                        };
                        _context.reports.Add(report);
                    }
                    _context.SaveChanges();
                    foreach (var item in list.reportList)
                    {
                        var updateReport = _context.reports.Single(r => r.category == item.category);
                        updateReport.total_quiz += item.total_quiz;
                        updateReport.correct_count += item.correct_count;
                        updateReport.incorrect_count += item.incorrect_count;
                    }
                    _context.SaveChanges();
                    dbTrans.Commit();
                    return Ok("Update the data complete.");
                }
                catch (Exception ex)
                {
                    dbTrans.Rollback();
                    return BadRequest(ex.Message);
                }
            }
        }
        [HttpGet]
        public IActionResult GetReport()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var reportList = (from r in _context.reports
                                orderby r.report_id
                                select r).ToList();
            return Ok(reportList);
        }
    }
}
