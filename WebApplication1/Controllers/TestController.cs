﻿using Microsoft.AspNetCore.Mvc;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Text.Json.Serialization;
using WebApplication1.DataBase.Context;
using WebApplication1.DataBase.Models;

namespace WebApplication1.Controllers
{
    public class TestController : ControllerBase
    {
        private readonly IHttpClientFactory _httpClientFactory;
        readonly ExampleDbContext _context;
        public TestController(IHttpClientFactory httpClientFactory, ExampleDbContext context)
        {
            _httpClientFactory = httpClientFactory;
            _context = context;
        }
        [HttpGet]
        public IActionResult Index(int amount, string category = "", string difficulty = "", string type = "", int choice = 4)
        {
            if (type == "boolean")
            {
                choice = 2;
            }
            var questionList = (from q in _context.quizzes
                                where (q.category == category || category == null) 
                                && (q.difficulty == difficulty || difficulty == null)
                                && (q.type == type || type == null)
                                select q).Take(amount).ToList();
            APIResult result = new APIResult();
            foreach(var question in questionList)
            {
                ResultModel model = new ResultModel(); 
                model.quiz_id = question.quiz_id;
                model.category = question.category;
                model.type = question.type;
                model.difficulty = question.difficulty;
                model.question = question.question;
                model.correct_answer = question.correct_answer;
                model.incorrect_answers = (from i in _context.incorrect_answers
                                          where i.quiz_id == question.quiz_id
                                          select new IncorrectModel { 
                                              incorrect_answers_id = i.incorrect_answers_id,
                                              wrong_choice = i.wrong_choice
                                          }).Take(choice-1).ToList();
                result.results.Add(model);
            }
            var rnd = new Random();
            var randomized = result.results.OrderBy(item => rnd.Next()).ToList();
            result.results = randomized;
            return Ok(result);
        }



        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var uri = "https://opentdb.com/api.php?amount=50";
            var httpClient = _httpClientFactory.CreateClient();
            var response = await httpClient.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                using var contentStream = await response.Content.ReadAsStreamAsync();
                APIResult result = await JsonSerializer.DeserializeAsync<APIResult>(contentStream);
                addToDatabase(result);
            }
            return Ok("Success!");
        }

        void addToDatabase(APIResult data)
        {
            using (var dbTrans = _context.Database.BeginTransaction())
            {
                try
                {
                    for (var i = 0; i < data.results.Count; i++)
                    {
                        var keep = data.results[i];
                        var checkDup = from d in _context.quizzes where d.question == keep.question select d;
                        if (checkDup.Count() > 0)
                        {
                            Console.WriteLine("There's a duplicate value in list.");
                        }
                        else
                        {
                            quiz test = new quiz()
                            {
                                category = keep.category,
                                type = keep.type,
                                difficulty = keep.difficulty,
                                question = keep.question,
                                correct_answer = keep.correct_answer
                            };
                            _context.Add(test);
                            _context.SaveChanges();
                            int id = test.quiz_id;
                            for (var j = 0; j < keep.incorrect_answers.Count; j++)
                            {
                                incorrect_answer wrong = new incorrect_answer()
                                {
                                    wrong_choice = keep.incorrect_answers[j].wrong_choice,
                                    quiz_id = id
                                };
                                _context.Add(wrong);
                                _context.SaveChanges();
                            }
                        }
                    }
                    dbTrans.Commit();
                }
                catch (Exception ex)
                {
                    dbTrans.Rollback();
                    Console.WriteLine(ex.Message);
                }
            }
        }
    }
}
