using Microsoft.EntityFrameworkCore;
using WebApplication1;
using WebApplication1.DataBase.Context;
using WebApplication1.DataBase.Models;
using Microsoft.Extensions.Configuration;

var builder = WebApplication.CreateBuilder(args);
string connString = builder.Configuration.GetConnectionString("Default");
var mySqlServerVersion = new MySqlServerVersion(new Version(8, 0, 27));
builder.Services.AddControllers();
builder.Services.AddDbContext<ExampleDbContext>(
        options => options.UseMySql(connString, mySqlServerVersion).EnableSensitiveDataLogging());
builder.Services.AddHttpClient();
builder.Services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
{
    builder.AllowAnyOrigin()
           .AllowAnyMethod()
           .AllowAnyHeader();
}));
builder.Services.AddTransient<Config>();
var app = builder.Build();

app.MapGet("/", () => "Hello World!");
app.UseCors("MyPolicy");
app.UseRouting();
app.MapControllerRoute(
    name: "default",
    pattern: "{controller}/{action}"
   );

app.Run();


//var serviceCollection = new ServiceCollection();
//serviceCollection.AddTransient<Config>();
//serviceCollection.AddTransient<Config2>();
//serviceCollection.AddTransient<Worker>();
//var serviceProvider = serviceCollection.BuildServiceProvider();
////var configSelect = serviceProvider.GetRequiredService<Config>();
//var worker = serviceProvider.GetRequiredService<Worker>();
////Worker worker = new Worker(new Config());
//worker.Work();
//;
public class Config
{
    public string ProjectName { get; set; } = "my project";
}

//public class Config2
//{
//    public string ProjectName { get; set; } = "my project";
//}